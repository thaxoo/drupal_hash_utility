from sys import argv

from drupal_hash_utility.cli import parse_args

if __name__ == "__main__":
    parse_args(*argv)
